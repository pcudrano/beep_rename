# Beep Rename #

Decodes names of beep downloaded files and folders + unzips recursively all subfolders.

## Usage ##
Make sure you already have Node.js.

Install dependencies with:

```
npm install 
```
Then run the script:

```
node rename.js <downloaded folder>|<downloaded zip file>

```