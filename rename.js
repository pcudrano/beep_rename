var fs = require( 'fs' );
var path = require( 'path' );
var process = require( 'process' );
// var unzip = require ('unzip');
// var Decompress = require('decompress');
var AdmZip = require('adm-zip');

var base = process.argv.slice(2)[0];

console.log("Working on %s", base);

fs.stat( base, function( error, stat ) {
	if(stat.isDirectory())
		renameFilesInDir(base);
	else if(stat.isFile() && path.extname(base) == ".zip") 
		unzipAndAnalyze(base)
	else
		console.log("Error: arg should be folder or zip file");
});

function renameFilesInDir (dir) {
	// Loop through all the files in the temp directory
	fs.readdir(dir, function( err, files ) {
		if( err ) {
			console.error( "Could not list the directory.", err );
			process.exit( 1 );
		} 

		files.forEach(function(file, index) {
			renameFile(file, index, dir);
		});
	} );
}

function renameFile( file, index , curDir) {
	// Make one pass and make the file complete
	var filename = path.basename(file);
	var dir = curDir;
	var fromPath = path.join( curDir, filename );
	
	// REMOVE ENCODINGS
	var newFileName = decodeURIComponent(filename.replace(/\+/g,"%20")).replace(/@/,"");
	
	var toPath = path.join( dir, newFileName );

	fs.stat( fromPath, function( error, stat ) {
		if( error ) {
			console.error( "Error stating file.", error );
			return;
		}

		var isDir = false;
		var isFile = false;
		if( stat.isFile() ) {
			console.log( "'%s' is a file.", fromPath );
			isFile = true;
		}
		else if( stat.isDirectory() ) {
			isDir = true;
			console.log( "'%s' is a directory.", fromPath );
		}

		fs.rename( fromPath, toPath, function( error ) {
			if( error ) {
				console.error( "File moving error.", error );
			}
			else {
				console.log( "Moved file '%s' to '%s'.", fromPath, toPath );
				if(isDir) {
					renameFilesInDir(toPath);
				} else if(isFile) {
					var ext = path.extname(toPath);
					if(ext == ".zip") {
						unzipAndAnalyze(toPath)
						fs.unlink(toPath);
					}
				}
			}
		} );
	} );
} 

function unzipAndAnalyze(zipFile) {
	const newFolderPath = zipFile.replace(/.zip$/,"");
	var zip = new AdmZip(zipFile);
	zip.extractAllTo(/*target path*/newFolderPath, /*overwrite*/true);
	console.log("Unzipped file %s", zipFile);
	renameFilesInDir(newFolderPath);
}